import os
import psycopg2

_conn = None

def connect():
    '''connects to database unless connection is already active'''
    global _conn
    try:
        with _conn.cursor() as cur:
            cur.execute('select 1')
    except:
        _conn = psycopg2.connect("host='%s' dbname='%s' user='%s' password='%s'" % (
            os.environ['DBHOST'], os.environ['DBNAME'],
            os.environ['DBUSER'], os.environ['DBPASS']))
        _conn.autocommit = True

def submission_seen(submission_id):
    '''checks database for submission_id'''
    with _conn.cursor() as cur:
        cur.execute('select count(*) from submissions where id = %s', [submission_id])
        return cur.fetchone()[0] > 0

def store_submission(submission_id):
    '''adds submission_id to database'''
    with _conn.cursor() as cur:
        cur.execute('insert into submissions (id) values (%s)', [submission_id])

def forget_submission(submission_id):
    '''removes submission_id from database'''
    with _conn.cursor() as cur:
        cur.execute('delete from submissions where submission_id = %s', [submission_id])

def comment_seen(comment_id):
    '''checks database for comment_id'''
    with _conn.cursor() as cur:
        cur.execute('select count(*) from comments where id = %s', [comment_id])
        return cur.fetchone()[0] > 0

def store_comment(comment_id):
    '''adds comment_id to database'''
    with _conn.cursor() as cur:
        cur.execute('insert into comments (id) values (%s)', [comment_id])

def forget_comment(comment_id):
    '''removes comment_id from database'''
    with _conn.cursor() as cur:
        cur.execute('delete from comments where comment_id = %s', [comment_id])
