import json
import os
import requests

_LDS_CHAPTER_LINK     = '**[%(book)s %(chapter)d](http://www.lds.org/scriptures/%(vol_url)s/%(book_url)s/%(chapter)d)**'
_LDS_VERSE_LINK       = '**[%(book)s %(chapter)d:%(verse)d](http://www.lds.org/scriptures/%(vol_url)s/%(book_url)s/%(chapter)d.%(verse)d#%(anchor)d)**'
_LDS_VERSE_RANGE_LINK = '**[%(book)s %(chapter)d:%(verse)d-%(endverse)d](http://www.lds.org/scriptures/%(vol_url)s/%(book_url)s/%(chapter)d.%(verse)d-%(endverse)d#%(anchor)d)**'

_JST_CHAPTER_LINK     = '**[%(book)s %(chapter)d (JST)](http://www.centerplace.org/hs/iv/default.htm)**'
_JST_VERSE_LINK       = '**[%(book)s %(chapter)d:%(verse)d (JST)](http://www.centerplace.org/hs/iv/default.htm)**'
_JST_VERSE_RANGE_LINK = '**[%(book)s %(chapter)d:%(verse)d-%(endverse)d (JST)](http://www.centerplace.org/hs/iv/default.htm)**'

_ESV_LINK             = '**[%(citation)s (ESV)](http://www.esvbible.org/%(citation)s)**'

# load title translation list into memory
with open('books/titles.json') as f:
    _titles = json.load(f)

# load all of the books into memory
# _books['canon']['volume']['book'][chapter][verse]
_books = {}
for canon in os.listdir('books'):
    cur_path = os.path.join('books', canon)
    if not os.path.isdir(cur_path):
        continue
    _books[canon] = {}
    for volume in os.listdir(cur_path):
        with open(os.path.join(cur_path, volume)) as f:
            _books[canon][volume[:-5]] = json.load(f)

def get_link(ref):
    '''return reddit formatted link to display before verse'''
    if ref['translation'] == 'lds':
        if not ref['verse']:
            return _LDS_CHAPTER_LINK % ref
        elif ref['verse'] == ref['endverse']:
            return _LDS_VERSE_LINK % ref
        else:
            return _LDS_VERSE_RANGE_LINK % ref
    elif ref['translation'] == 'jst':
        if not ref['verse']:
            return _JST_CHAPTER_LINK % ref
        if ref['verse'] == ref['endverse']:
            return _JST_VERSE_LINK % ref
        else:
            return _JST_VERSE_RANGE_LINK % ref
    elif ref['translation'] == 'esv':
        return _ESV_LINK % ref

def get_verses(ref):
    '''return the text for the verses in @ref'''
    if ref['translation'] == 'esv':
        return _get_esv_verses(ref)
    try:
        book_ref = _titles['ref'][ref['book']]
        ref['book'] = _titles['title'][book_ref]
        ref['title'] = book_ref
        (vol, book) = book_ref.split('/')
        ref['vol_url'] = vol
        ref['book_url'] = book
        chapter = (_books[ref['translation']]
                    [vol][book]
                    [ref['chapter']])
        if ref['verse']:
            first_verse = ref['verse']
            verses = chapter[ref['verse']:ref['endverse']+1]
        else:
            first_verse = 1
            verses = chapter[1:]
        if not verses:
            print('Verses out of range: %s' % str(ref))
            return None
        return ['> %d %s' % (verse_num, verse_text)
            for (verse_num, verse_text) in enumerate(verses, first_verse)]
    except KeyError:
        print('Unable to find verse for reference: %s' % str(ref))
        return None

def _get_esv_verses(ref):
    '''return text from esvapi.org query'''
    query_url = ('http://www.esvapi.org/v2/rest/passageQuery?key=IP'
                    '&include-short-copyright=1'
                    '&include-passage-horizontal-lines=0'
                    '&include-heading-horizontal-lines=0'
                    '&output-format=plain-text'
                    '&line-length=0'
                    '&include-footnotes=0'
                    '&passage=%s')
    query_passage = '%(book)s+%(chapter)s' % ref
    if ref['verse']:
        query_passage += ':%(verse)d-%(endverse)d' % ref
    response = requests.get(query_url % query_passage, timeout=2)
    if not response.ok:
        print('HTTP Error (%d): %s' % (response.status_code, str(ref)))
        return None
    if response.text.startswith('ERROR'):
        print('%s: %s' % (response.text, str(ref)))
        return None
    (ref['citation'], verse_text) = response.text.split('\n', 1)
    return ['> %s' % line.strip() for line in verse_text.split('\n')]

