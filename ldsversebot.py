import database
import verses

from xml.sax.saxutils import unescape

import os
import praw
import re
import time

_REF_MATCH = re.compile("\[(?P<book>[- &'\w]{1,22}).? (?P<chapter>\d{1,3})(:(?P<verse>\d{1,3})(-(?P<endverse>\d{1,3}))?)?( (?P<translation>\w{2,3}))?\]")

def main():
    '''LDSVerseBot main loop - read/parse comments and respond'''
    r = praw.Reddit(user_agent = ('LDSVerseBot 0.2 by /u/amertune '
                                  'bitbucket.org/amertune/ldsversebot'))
    r.login('LDSVerseBot', os.environ['REDDIT_PASS'])

    while True:
        database.connect()
        # respond to submissions
        subreddit = r.get_subreddit(os.environ['REDDIT_SUBS'])
        for submission in subreddit.get_new(limit=25):
            if (not database.submission_seen(submission.id)
                and submission.author.name != 'LDSVerseBot'
                and submission.is_self):
                database.store_submission(submission.id)
                references = get_refs_from_comment(unescape(submission.selftext))
                message = build_message(references)
                if message:
                    print('posting reply (submission): %s' % submission.id)
                    submission.add_comment(message)
        # respond to comments
        for comment in subreddit.get_comments(limit=25):
            if not database.comment_seen(comment.id) and comment.author.name != 'LDSVerseBot':
                database.store_comment(comment.id)
                references = get_refs_from_comment(unescape(comment.body))
                message = build_message(references)
                if message:
                    print('posting reply (comment): %s' % comment.id)
                    comment.reply(message)
        time.sleep(30)

def _format_ref(ref):
    '''format a reference dict to make it usable for verse lookups
    everything should be in lower case, and chapter/verse should be ints'''
    if not ref['translation']:
        ref['translation'] = 'lds'
    ref['translation'] = ref['translation'].lower()
    if ref['translation'] == 'iv':
        ref['translation'] = 'jst'
    ref['book'] = ref['book'].lower()
    ref['chapter'] = int(ref['chapter'])
    if ref['verse']:
        ref['verse'] = int(ref['verse'])
        ref['anchor'] = ref['verse'] - 1
        if ref['endverse']:
            ref['endverse'] = int(ref['endverse'])
            if ref['endverse'] < ref['verse']:
                ref['endverse'] = ref['verse']
        else:
            ref['endverse'] = ref['verse']
    return ref

def get_refs_from_comment(comment_body):
    '''returns reference dicts for all scripture references in the comment'''
    return [_format_ref(match.groupdict())
        for match in _REF_MATCH.finditer(comment_body)]

def build_message(refs, char_limit=6000):
    '''expand the references into verse citations

    The message body will be truncated if it exceeds char_limit characters,
    and will include a standard footer (and truncation message if appropriate).
    '''
    truncate_msg = "\nThe cited verses exceed the character limit (%d). Truncating message.\n\n" % char_limit
    footer = ('\n\n----\n'
             '[Source](https://bitbucket.org/amertune/ldsversebot) - '
             '[Copyright Information](https://bitbucket.org/amertune/ldsversebot/wiki/Copyright%20Information) - '
             '[Help](https://bitbucket.org/amertune/ldsversebot/wiki/Home)')
    verse_char_limit = char_limit - (len(truncate_msg) + len(footer))
    message_body = []
    for ref in refs:
        verse_list = verses.get_verses(ref)
        if not verse_list:
            continue
        message_body.append(verses.get_link(ref))
        message_body.extend(verse_list)
    if not message_body:
        return None
    message = '\n\n'.join(message_body)
    if len(message) > verse_char_limit:
        message = message[:verse_char_limit].rsplit('\n', 1)[0] + truncate_msg
    message += footer
    return message

if __name__ == '__main__':
    main()
