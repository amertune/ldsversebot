import ldsversebot

import unittest

# [book chapter:verse-endverse translation]
class TestReferenceMatch(unittest.TestCase):
    def test_simple_match(self):
        m = ldsversebot._REF_MATCH.match('[x 1]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': 'x', 'chapter': '1',
             'verse': None, 'endverse': None,
             'translation': None})

    def test_numbered_book_name(self):
        m = ldsversebot._REF_MATCH.match('[1 x 1]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': '1 x', 'chapter': '1',
             'verse': None, 'endverse': None,
             'translation': None})

    def test_abbreviated_book_name(self):
        m = ldsversebot._REF_MATCH.match('[xxx. 1]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': 'xxx', 'chapter': '1',
             'verse': None, 'endverse': None,
             'translation': None})

    def test_long_book_name(self):
        m = ldsversebot._REF_MATCH.match('[9 xxx xxxxxxxx x xxx 2 1]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': '9 xxx xxxxxxxx x xxx 2', 'chapter': '1',
             'verse': None, 'endverse': None,
             'translation': None})

    def test_verse_match(self):
        m = ldsversebot._REF_MATCH.match('[x 1:1]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': 'x', 'chapter': '1',
             'verse': '1', 'endverse': None,
             'translation': None})

    def test_verse_range_match(self):
        m = ldsversebot._REF_MATCH.match('[x 1:1-2]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': 'x', 'chapter': '1',
             'verse': '1', 'endverse': '2',
             'translation': None})

    def test_translation_match(self):
        m = ldsversebot._REF_MATCH.match('[x 1 xxx]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': 'x', 'chapter': '1',
             'verse': None, 'endverse': None,
             'translation': 'xxx'})

    def test_complex_match(self):
        m = ldsversebot._REF_MATCH.match('[9 xYz&zyX-xxZZ& 7 1:5-7 ZzX]')
        self.assertIsNotNone(m)
        self.assertEqual(m.groupdict(),
            {'book': '9 xYz&zyX-xxZZ& 7', 'chapter': '1',
             'verse': '5', 'endverse': '7',
             'translation': 'ZzX'})

class TestFormatReference(unittest.TestCase):
    def test_simple_format(self):
        ref = {'book': 'xXx', 'chapter': '1',
               'verse': None, 'endverse': None,
               'translation': 'XXX'}
        self.assertEqual(ldsversebot._format_ref(ref),
            {'book': 'xxx', 'chapter': 1,
             'verse': None, 'endverse': None,
             'translation': 'xxx'})

    def test_default_translation(self):
        ref = {'book': 'xXx', 'chapter': '1',
               'verse': None, 'endverse': None,
               'translation': None}
        self.assertEqual(ldsversebot._format_ref(ref),
            {'book': 'xxx', 'chapter': 1,
             'verse': None, 'endverse': None,
             'translation': 'lds'})

    def test_iv_treated_as_jst(self):
        ref = {'book': 'xXx', 'chapter': '1',
               'verse': None, 'endverse': None,
               'translation': 'iv'}
        self.assertEqual(ldsversebot._format_ref(ref),
            {'book': 'xxx', 'chapter': 1,
             'verse': None, 'endverse': None,
             'translation': 'jst'})

    def test_verse(self):
        ref = {'book': 'xXx', 'chapter': '1',
               'verse': '1', 'endverse': None,
               'translation': None}
        self.assertEqual(ldsversebot._format_ref(ref),
            {'book': 'xxx', 'chapter': 1,
             'verse': 1, 'endverse': 1, 'anchor': 0,
             'translation': 'lds'})

    def test_verse(self):
        ref = {'book': 'xXx', 'chapter': '1',
               'verse': '1', 'endverse': None,
               'translation': None}
        self.assertEqual(ldsversebot._format_ref(ref),
            {'book': 'xxx', 'chapter': 1,
             'verse': 1, 'endverse': 1, 'anchor': 0,
             'translation': 'lds'})

    def test_endverse(self):
        ref = {'book': 'xXx', 'chapter': '1',
               'verse': '1', 'endverse': '2',
               'translation': None}
        self.assertEqual(ldsversebot._format_ref(ref),
            {'book': 'xxx', 'chapter': 1,
             'verse': 1, 'endverse': 2, 'anchor': 0,
             'translation': 'lds'})

class TestBuildMessage(unittest.TestCase):
    def test_long_message(self):
        message = ldsversebot.build_message([{'book': 'psalms', 'chapter': 119,
                                              'verse': None, 'translation': 'lds'}],
                                            500)
        self.assertIn('The cited verses exceed the character limit (500). Truncating message.', message)
        self.assertTrue(len(message) < 500)
